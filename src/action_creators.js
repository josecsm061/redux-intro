const createTweet = (data = [], username) => {
    return {
        type: "CREATE_TWEET",
        payload: {
            id: data.id,
            text: data.text,
            username: username,
            createdAt: new Date().getTime()
        }
    }
};

const editTweet = (id, text) => {
    return {
        type: "EDIT_TWEET",
        payload: {
            id: id,
            text: text,
            updatedAt: new Date().getTime()
        }
    }
}

const deleteTweet = (id) => {
    return {
        type: "DELETE_TWEET",
        payload: {
            id: id
        }
    }
}

module.exports = {
    createTweet: createTweet,
    editTweet: editTweet,
    deleteTweet: deleteTweet
}