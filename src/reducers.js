
const tweets = (data = [], action) => {
    if (action.type === "CREATE_TWEET") {
        return [...data,
        action.payload];
    } else if (action.type === "EDIT_TWEET") {
        data[0].text = action.payload.text;
        return data;
    } else if (action.type === "DELETE_TWEET") {
        return data.filter(tweet => tweet.id !== action.payload.id);
    }
    return data;
}


module.exports = {
    tweets: tweets
}
