const reducers = require("../../src/reducers");
const actions = require("../../src/action_creators");

test("Sprint 3 | It should be defined", () => {
  expect(reducers.tweets).toBeDefined();
});

test("Sprint 3 | It return an empty list as base case", () => {
  const actual = reducers.tweets(undefined, { type: "DUMMY_TWEET" });

  expect(actual).toEqual([]);
});

test("Sprint 3 | It can handle CREATE_TWEET", () => {
  const action = actions.createTweet({ id: 1, text: "First tweet." }, "mr_robot");
  const actual = reducers.tweets(undefined, action);
  const expected = {
    id: 1,
    text: "First tweet.",
    username: "mr_robot",
    createdAt: new Date().getTime()
  };

  expect(actual.length).toBe(1);
  expect(actual[0].id).toEqual(expected.id);
  expect(actual[0].text).toEqual(expected.text);
  expect(actual[0].username).toEqual(expected.username);
  expect(actual[0]).toHaveProperty("createdAt");
});

test("Sprint 3 | It can handle CREATE_TWEET twice", () => {
  const initialState = [{
    id: 1,
    text: "First tweet.",
    username: "mr_robot",
    createdAt: 1553920667978
  }];

  const action = actions.createTweet({ id: 2, text: "Second tweet!" }, "mr_robot");
  const actual = reducers.tweets(initialState, action);
  const expected = {
    id: 2,
    text: "Second tweet!",
    username: "mr_robot",
    createdAt: new Date().getTime()
  };

  expect(actual.length).toBe(2);
  expect(actual[0]).toEqual(initialState[0]);
  expect(actual[1].id).toEqual(expected.id);
  expect(actual[1].text).toEqual(expected.text);
  expect(actual[1].username).toEqual(expected.username);
  expect(actual[1]).toHaveProperty("createdAt");
});

test("Sprint 3 | It can handle EDIT_TWEET", () => {
  const initialState = [{
    id: 1,
    text: "First tweet.",
    username: "mr_robot",
    createdAt: 1553920667978
  }];

  const action = actions.editTweet(1, "Edited seconds ago.");
  const actual = reducers.tweets(initialState, action);
  const expected = {
    id: 1,
    text: "Edited seconds ago.",
    username: "mr_robot",
    createdAt: 1553920667978
  };

  expect(actual.length).toBe(1);
  expect(actual[0]).toEqual(expected);
});

test("Sprint 3 | It can handle DELETE_TWEET", () => {
  const initialState = [{
    id: 1,
    text: "First tweet.",
    username: "mr_robot",
    createdAt: 1553920667978
  }];

  const action = actions.deleteTweet(1);
  const actual = reducers.tweets(initialState, action);
  const expected = [];

  expect(actual.length).toBe(0);
  expect(actual).toEqual(expected);
});
