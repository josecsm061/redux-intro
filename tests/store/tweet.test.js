const store = require("../../src/store");
const reducers = require("../../src/reducers");
const actions = require("../../src/action_creators");

test("Sprint 4 | Use `combineReducers` to create the Store", () => {
  expect(store.dispatch).toBeDefined();
});

test("Sprint 4 | Store is working when user create a Tweet", () => {
  const { createStore, combineReducers } = require("redux");
  const store = createStore(combineReducers(reducers));

  store.dispatch(actions.createTweet({ id: 1, text: "Hey! Welcome!" }, "mr_robot"));
  const expected = {
    tweets: [{
      id: 1,
      text: "Hey! Welcome!",
      username: "mr_robot",
      createdAt: new Date().getTime()
    }]
  };

  const state = store.getState();

  expect(state).toHaveProperty("tweets");
  expect(state.tweets).toBeDefined();
  expect(state.tweets.length).toBe(1);
  expect(state.tweets[0]).toHaveProperty("id");
  expect(state.tweets[0]).toHaveProperty("text");
  expect(state.tweets[0]).toHaveProperty("username");
  expect(state.tweets[0]).toHaveProperty("createdAt");
});

test("Sprint 4 | Store is working when user create a Tweet twice", () => {
  const { createStore, combineReducers } = require("redux");
  const store = createStore(combineReducers(reducers));

  store.dispatch(actions.createTweet({ id: 1, text: "It works!" }, "mr_robot"));
  store.dispatch(actions.createTweet({ id: 2, text: "What’s happening!?" }, "mr_robot"));

  const expected = {
    tweets: [{
      id: 1,
      text: "It works!",
      username: "mr_robot",
      createdAt: new Date().getTime()
    }, {
      id: 2,
      text: "What’s happening!?",
      username: "mr_robot",
      createdAt: new Date().getTime()
    }]
  };

  const state = store.getState();

  expect(state).toHaveProperty("tweets");
  expect(state.tweets.length).toBe(2);
  expect(state.tweets[0]).toHaveProperty("id");
  expect(state.tweets[0]).toHaveProperty("text");
  expect(state.tweets[0]).toHaveProperty("username");
  expect(state.tweets[0]).toHaveProperty("createdAt");
  expect(state.tweets[1]).toHaveProperty("id");
  expect(state.tweets[1]).toHaveProperty("text");
  expect(state.tweets[1]).toHaveProperty("username");
  expect(state.tweets[1]).toHaveProperty("createdAt");

  expect(state.tweets[0].id).toBe(1);
  expect(state.tweets[0].text).toBe("It works!");
  expect(state.tweets[0].username).toBe("mr_robot");
  expect(state.tweets[0]).toHaveProperty("createdAt");
  expect(state.tweets[1].id).toBe(2);
  expect(state.tweets[1].text).toBe("What’s happening!?");
  expect(state.tweets[1].username).toBe("mr_robot");
  expect(state.tweets[1]).toHaveProperty("createdAt");
});

test("Sprint 4 | Store is working when user edit a Tweet", () => {
  const { createStore, combineReducers } = require("redux");
  const store = createStore(combineReducers(reducers));

  store.dispatch(actions.createTweet({ id: 1, text: "It works!" }, "mr_robot"));
  store.dispatch(actions.editTweet(1, "Finally, it works!"));

  const expected = {
    tweets: [{
      id: 1,
      text: "Finally, it works!",
      username: "mr_robot",
      createdAt: new Date().getTime()
    }]
  };
  const state = store.getState();

  expect(state).toHaveProperty("tweets");
  expect(state.tweets).toBeDefined();
  expect(state.tweets.length).toBe(1);
  expect(state.tweets[0]).toHaveProperty("id");
  expect(state.tweets[0]).toHaveProperty("text");
  expect(state.tweets[0]).toHaveProperty("username");
  expect(state.tweets[0]).toHaveProperty("createdAt");

  expect(state.tweets[0].id).toBe(expected.tweets[0].id);
  expect(state.tweets[0].text).toEqual(expected.tweets[0].text);
  expect(state.tweets[0].username).toEqual(expected.tweets[0].username);
  expect(state.tweets[0].createdAt).toBeDefined();
});

test("Sprint 4 | Store is working when user delete a Tweet", () => {
  const { createStore, combineReducers } = require("redux");
  const store = createStore(combineReducers(reducers));

  store.dispatch(actions.createTweet({ id: 1, text: "It works!" }, "mr_robot"));
  store.dispatch(actions.deleteTweet(1));

  const expected = {
    tweets: []
  };

  const state = store.getState();

  expect(state).toHaveProperty("tweets");
  expect(state.tweets).toBeDefined();
  expect(state.tweets.length).toBe(0);
});

test("Sprint 4 | Store is working when user delete two Tweets", () => {
  const { createStore, combineReducers } = require("redux");
  const store = createStore(combineReducers(reducers));

  store.dispatch(actions.createTweet({ id: 1, text: "It works!" }, "mr_robot"));
  store.dispatch(actions.createTweet({ id: 2, text: "Finally!!!" }, "mr_robot"));

  expect(store.getState().tweets.length).toBe(2);

  store.dispatch(actions.deleteTweet(1));
  expect(store.getState().tweets.length).toBe(1);

  store.dispatch(actions.deleteTweet(2));
  expect(store.getState().tweets.length).toBe(0);

  const expected = {
    tweets: []
  };

  const state = store.getState();

  expect(state).toHaveProperty("tweets");
  expect(state.tweets).toBeDefined();
  expect(state.tweets.length).toBe(0);

});
