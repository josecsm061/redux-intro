const actions = require("../../src/action_creators");

test("Sprint 2 | It should return type and payload", () => {
  const actual = actions.deleteTweet(1);

  expect(actual).toHaveProperty("type");
  expect(actual).toHaveProperty("payload");
});

test("Sprint 2 | It should return DELETE_TWEET as type", () => {
  const actual = actions.deleteTweet(1);

  expect(actual.type).toEqual("DELETE_TWEET");
});

test("Sprint 2 | It should return an object as payload", () => {
  const actual = actions.deleteTweet(1);

  expect(actual.payload).toBeDefined();
});

test("Sprint 2 | It should the correct payload object", () => {
  const actual = actions.deleteTweet(1);

  expect(actual.payload).toHaveProperty("id");
});

test("Sprint 2 | It should match the whole object", () => {
  const actual = actions.deleteTweet(1);

  const expected = {
    type: "DELETE_TWEET",
    payload: {
      id: 1
    }
  };

  expect(actual).toEqual(expected);
});
