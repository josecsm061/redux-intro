const actions = require("../../src/action_creators");

test("Sprint 2 | It should return type and payload", () => {
  const actual = actions.editTweet(1, "I like it!!!");

  expect(actual).toHaveProperty("type");
  expect(actual).toHaveProperty("payload");
});

test("Sprint 2 | It should return EDIT_TWEET as type", () => {
  const actual = actions.editTweet(1, "I like it!!!");

  expect(actual.type).toEqual("EDIT_TWEET");
});

test("Sprint 2 | It should return an object as payload", () => {
  const actual = actions.editTweet(1, "I like it!!!");

  expect(actual.payload).toBeDefined();
});

test("Sprint 2 | It should the correct payload object", () => {
  const actual = actions.editTweet(1, "I like it!!!");

  expect(actual.payload).toHaveProperty("id");
  expect(actual.payload).toHaveProperty("text");
  expect(actual.payload).toHaveProperty("updatedAt");
});

test("Sprint 2 | It should match the whole object", () => {
  const actual = actions.editTweet(1, "I like it!!!");

  const expected = {
   type: "EDIT_TWEET",
    payload: {
      id: 1,
      text: "I like it!!!",
      updatedAt: new Date().getTime()
    }
  };

  expect(actual.type).toEqual(expected.type);
  expect(actual.payload.id).toEqual(expected.payload.id);
  expect(actual.payload.text).toEqual(expected.payload.text);
  expect(actual.payload.updatedAt).toEqual(expected.payload.updatedAt);
});
