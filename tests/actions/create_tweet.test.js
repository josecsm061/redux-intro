const actions = require("../../src/action_creators");

test("Sprint 2 | It should return type and payload", () => {
  const actual = actions.createTweet({ id: 1, text: "My first tweet" }, "mr_robot");

  expect(actual).toHaveProperty("type");
  expect(actual).toHaveProperty("payload");
});

test("Sprint 2 | It should return CREATE_TWEET as type", () => {
  const actual = actions.createTweet({ id: 1, text: "My first tweet" }, "mr_robot");

  expect(actual.type).toEqual("CREATE_TWEET");
});

test("Sprint 2 | It should return an object as payload", () => {
  const actual = actions.createTweet({ id: 1, text: "My first tweet" }, "mr_robot");

  expect(actual.payload).toBeDefined();
});

test("Sprint 2 | It should the correct payload object", () => {
  const actual = actions.createTweet({ id: 1, text: "My first tweet" }, "mr_robot");

  expect(actual.payload).toHaveProperty("id");
  expect(actual.payload).toHaveProperty("username");
  expect(actual.payload).toHaveProperty("text");
  expect(actual.payload).toHaveProperty("createdAt");
});

test("Sprint 2 | It should match the whole object", () => {
  const actual = actions.createTweet({ id: 1, text: "My first tweet" }, "mr_robot");
  const expected = {
   type: "CREATE_TWEET",
    payload: {
      id: 1,
      username: "mr_robot",
      text: "My first tweet",
      createdAt: new Date().getTime()
    }
  };

  expect(actual.type).toEqual(expected.type);
  expect(actual.payload.id).toEqual(expected.payload.id);
  expect(actual.payload.username).toEqual(expected.payload.username);
  expect(actual.payload.text).toEqual(expected.payload.text);
  expect(actual.payload.createdAt).toEqual(expected.payload.createdAt);
});
